syntax = "proto3";
import "common.proto";
import "admin/coverage.proto";
package com.freeje.api.admin;

option java_multiple_files = false;
option java_package = "com.freeje.api.admin";
option java_outer_classname = "DIDType";
option objc_class_prefix = "DIDType";
option go_package = "gitlab.com/BetaCompany/Freeje/sdk/go/api/admin;freeje_admin";

// Сервис управления подключением номеров
service DID {

  /* Получить список всех DID номеров по фильру, если фильтр пустой то будет выдан полный перечень */
  rpc ListProviders (SearchFilter) returns (stream DidProvider);

  rpc UpdateProvider (DidProvider) returns (DidProvider);
  rpc SyncProvider (DidProvider) returns (stream Empty);

  rpc GetProvider (DidProvider) returns (DidProvider);

  rpc DeleteProvider (DidProvider) returns (DidProvider);

  /* Получить список всех DID номеров по фильру, если фильтр пустой то будет выдан полный перечень */
  rpc List (SearchFilter) returns (stream DidNumber);

  /* Получить текущие значения для DID  */
  rpc Get (DidNumber) returns (DidNumber);

  /* Мониторить параметры конкретного DID  */
  rpc Monitor(DidNumber) returns (stream DidNumber);

  /* Получить текущие значения для DID  */
  rpc GetLog (SearchFilter) returns (stream DidLog);

  /* Получить текущие значения для DidRequest  */
  rpc GetRequest (DidRequest) returns (DidRequest);

  /* Получить подтверждающие пользователя документы прикрепленные к номеру  */
  rpc GetDidDocs (DidNumber) returns (stream DidDocument);

  /* Добавить или обновить информацию по DID номеру
   * При добавлении uid генерируется и возвращается автоматически
   */
  rpc Update (DidNumber) returns (DidNumber);

  /* Получает список всех активных запросов и их последующих изменений */
  rpc GetRequests (RequestMode) returns (stream DidRequest);

  /* Отклоняет позьзовательский запрос на подключение номера */
  rpc Reject (DidRequest) returns (DidRequest);

  /* Удаляет DID если это возможно
   * при удалении передается объект с единственным заполненным полем id
  */
  rpc Delete (DidNumber) returns (Empty);

  /* Ищет подходящую зону покрытия для номера*/
  rpc AssumeCoverage (DidNumber) returns (CoverageArea);

  /* Назначает акаунту номер из пула, при этом не важно занят он кем лдибо или нет.
  Если занят то будет отобран у текужего владельца.
  */
  rpc AssignDid (DidAssignment) returns (DidAssignment);

  //Получить список возможных номеров для покупки
  rpc ListAvailableNumbers (DidListRequest) returns (stream DidNumber);

  //Выполнить запрос на подключение номера
  rpc ApplyDidRequest (DidRequest) returns (DidRequest);

}

service DidPriceManager {
  rpc List (RequestMode) returns (stream DidPriceOverride);
  rpc Update (DidPriceOverride) returns (DidPriceOverride);
  rpc Delete (DidPriceOverride) returns (DidPriceOverride);
  rpc UpdatePrefix (DidPrefixPrice) returns (DidPrefixPrice);
  rpc DeletePrefix (DidPrefixPrice) returns (DidPrefixPrice);
  rpc ListPrefix (DidPriceOverride) returns (stream DidPrefixPrice);
}

message DidRequest {
  string id = 1;
  string owner_id = 2;
  string owner_name = 3;
  string owner_email = 4;
  DidNumber did = 5;
  DidStatus status = 6;
  int64 createdms = 7; // время создания запроса
  string error = 8;
  repeated string document_id = 9; // идентификаторы данных документа при покупке

}

message DidAssignment {
  string did_id = 1;
  string account_id = 2;
  int64 payed_until = 3;
}

message DidDocument {
  string id = 1; // идентификатор документа
  int64 created = 2; // время документа
  DocumentType type = 3;
  bytes data = 4; // содержимое документа в зависимости от DocumentType
  string label = 5; // краткое обозначение
}

message DidListRequest {
  string prefix = 1;
  string account_id = 2;
}

enum DidLogType {
  ATTACH = 0; // включение в пул (покупка) номера с провайдера
  ASSIGN = 1; // назначение номера пользователю
  REASSIGN = 2; //  переназначение (продление) номера пользователю
  RELEASE = 3; // особождение назначения номера пользователю
  DETACH = 4; // исключение из пула (возврат) номера с провайдера
  EDIT = 5; // номер отредактирован
}


message DidLog {
  string id = 1;
  int64 stamp = 2; // время документа
  DidLogType type = 3;
  DidNumber did = 4;
  string diff = 5;
  string authorEmail = 6;
}

message DidProvider {
  enum Type {
    DIDX = 0;
    NEXMO = 1;
    DIDWW = 2;
    SIPMARKET = 3;
    ANVEO = 4;
    TWILLIO = 5;
    FOLLOWGRAM = 6;
    SMSHUB = 7;
    GETSMSCODE = 8;
    SIMSMS = 9;
  }
  string id = 1;
  string name = 2;
  Type type = 3;
  repeated string auth = 4; //массив данных для авторизации (логин пароль ключ и тд)
  int64 createms = 5; // время создания
  int64 updatems = 6; // время последнего обновления
  int64 syncms = 7; // время синхронизации номеров
  int64 release_delay_sec = 8; // дефолтное время задержки освобождения номера на провайдере в секундах (-1 никогда)
  string notes = 9;
  bool use_in_offer = 10; // использовать провайдера для выдачи предложений
  string price_id = 11;
  int32 timeout_sec = 12; // максимальное время не ответ провайдера в секундах (0 без ограничений)
  string currency = 13; // валюта оператора , если на задана то USD
}

message DidNumber {
  string id = 1; // идентификатор (может быть пустым если это не конкретный купленный номер
  string number = 2; // номер или его префикс
  string did_name = 3; // Читабельный идентификатор оператора
  string country = 4; // ISO-2 код страны номера
  int64 payed_till = 5;
  string provider_id = 6; // идентификатор оператора
  string provider = 7; // идентификатор оператора
  int64 createdms = 10; // время создания номера
  int64 assignedms = 11; // время последнего присвоения
  string locality = 12; // Местонахождение
  string localityPrefix = 13; // префикс по которому вычислено местонахождение
  PhoneType type = 14;
  bool available = 15;
  DidPrice vendorPrice = 16;
  DidPrice customerPrice = 17;
  string notes = 18;
  bool owned = 19;
  string owner_id = 20;
  repeated DocumentType requirements = 21; // типы документов которые необходимо предоставить
  repeated NumberFeature features = 22; // особенности номеров в зоне
  string owner_name = 23; // Имя пользователя
  string owner_email = 24; // Email пользователя
  int64 release_delay_sec = 25; // время задержки освобождения номера на провайдере в секундах (-1 никогда)
}

message DidPriceOverride {
  string id = 1; // идентификатор
  string name = 2; // название
  bool  deleted = 3; // признак удаления
}
message DidPrefixPrice {
  enum Mode {
    NEVER = 0; // никогда
    GREATER = 1; //если цена больше чем заявлено у оператора
    ALWAYS = 2; // всегда
    EXCLUDE = 3; // исключить из выдачи
  }
  string did_price_id = 1;
  string prefix = 2; // префикс
  Mode mode = 3; // условия переопределения цены
  double setup_fee = 4; // стоимость выделения номера
  double monthly_fee = 5; // ежемесячное содержание номера
  string comment = 6; // комментарий
  int64 updated = 7; // время последнего обновления, -1 удален
  double incoming_minute_rate = 10; // цена минуты входящего вызова
}

